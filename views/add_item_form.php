<?php  
	require '../templates/template.php';
	function get_content(){
	require '../controllers/connection.php'
?>
	<h1 class="text-center py-5">Add Item Form</h1>
	<div class="container">
		<div class="col-lg-6 offset-log-3">
			<form action="../controllers/process_add_item.php" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<label for="name">Item Name</label>
					<input type="text" name="name" class="form-control" placeholder="Item Name">
				</div>
				<div class="form-group">
					<label for="price">Item Price</label>
					<input type="number" name="price" class="form-control">
				</div>		
				<div class="form-group">
					<label for="description">Item Description</label>
					<textarea name="description" class="form-control" placeholder="description..."></textarea>
				</div>
				<div class="form-group">
					<label for="image">Item Image</label>
					<input type="file" name="image" class="form-control"></input>
				</div>
				<div class="form-group">
					<label for="category_id">Category:</label>
					<select name="category_id" class="form-control">
						<?php  
						$category_query = 'SELECT * FROM categories';
						$category = mysqli_query($conn, $category_query);
						foreach($category as $indiv_category){
						?>
						 <option value="<?php echo $indiv_category['id'] ?>"><?php echo $indiv_category['name'] ?></option>
						<?php	
						}
						?>
					</select>
				</div>
				<button type="submit" class="btn btn-success">Add Item</button>
			</form>
		</div>
	</div>




<?php  
	}
?>