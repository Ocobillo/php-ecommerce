<?php 
	require "../templates/template.php";
	function get_content(){
	require "../controllers/connection.php";
?>
	<h1 class="text-center py-5">CATALOG PAGE</h1>
	<div class="container">
		<div class="row">
		<?php  
		// steps for retrieving items 
		//1.Create a query 
		//2.Use mysqli_query to get the results
		//3. If array (use foreach)
		//4.If object (use mysqli_fetch_assoc) to convert the data to an associative array
		//4.1 User foreach ($result as $key => $value)

		$items_query = "SELECT * FROM items";
		$items = mysqli_query($conn, $items_query);
		foreach ($items as $indiv_item){
		?>

		<div class="col-lg-4 py-2">
			<div class="card h-100">
				<img alt="image" class="card-img-top" src="<?php echo $indiv_item['image'] ?>">
				<div class="card-body">
					<h4 class="card-title"><?php echo $indiv_item['name'] ?></h4>
					<p class="card-text">Php<?php echo $indiv_item['price'] ?></p>
					<p class="card-text"><?php echo $indiv_item['description'] ?></p>
					<?php  
						//process of displaying category
						//1. Get categories where id is equal to $indiv_item['category_id']
						//2. display the data
					$cat_id = $indiv_item['category_id'];
					$category_query = "SELECT * FROM categories WHERE id = $cat_id";
					$category = mysqli_fetch_assoc(mysqli_query($conn, $category_query));
					?>
					<p class="card-text">Category: <?php echo $category['name'] ?></p>
				</div>
				<div class="card-footer"><a href="edit_item_form.php?id=<?php echo $indiv_item['id']?>" class="btn btn-secondary">Edit Item</a></div>
			</div>
		</div>



	<?php 	
	}
	?>
		</div>
	</div>



<?php 
	}
?>