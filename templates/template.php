<!DOCTYPE html>
<html>
<head>
	<title>Fancy</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cyborg/bootstrap.css">
</head>
<body>
	<?php require "navbar.php";
		  get_content();
		  require "footer.php";
	 ?>
</body>
</html>